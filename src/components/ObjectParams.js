import React from 'react'
import { Input } from 'reactstrap'

class ObjectParams extends React.Component {
    componentDidMount() {
          
    }
    onChangeX(e){
     var copy = this.props.objects;
     copy[this.props.currentActiveIndex].x = +e.target.value;
    this.props.updateData(copy);
 }
    onChangeY(e){
      var copy = this.props.objects;
      copy[this.props.currentActiveIndex].y = +e.target.value;
      this.props.updateData(copy);
    }
    onChangeH(e){
      var copy = this.props.objects;
      copy[this.props.currentActiveIndex].h = +e.target.value;
      this.props.updateData(copy);
    }
    onChangeW(e){
      var copy = this.props.objects;
      copy[this.props.currentActiveIndex].w = +e.target.value;
      this.props.updateData(copy);
    }
     ChangeColor(color){
       var copy = this.props.objects;
      copy[this.props.currentActiveIndex].color = color;
      this.props.updateData(copy);
     }
        
     
  render() {
     var valx;
          if(this.props.objects[this.props.currentActiveIndex].x !== 0){
            valx = this.props.objects[this.props.currentActiveIndex].x;
          } else {
            valx = '';
          }
      var valy;
          if(this.props.objects[this.props.currentActiveIndex].y !== 0){
            valy = this.props.objects[this.props.currentActiveIndex].y;
          } else {
            valy = '';
          }
     var valh;
          if(this.props.objects[this.props.currentActiveIndex].h !== 0){
            valh = this.props.objects[this.props.currentActiveIndex].h;
          } else {
            valh = '';
          }
     var valw;
          if(this.props.objects[this.props.currentActiveIndex].w !== 0){
            valw = this.props.objects[this.props.currentActiveIndex].w;
          } else {
            valw = '';
          }
        

    return (
       <div className="col-lg-2">
        <h3>{this.props.objects[this.props.currentActiveIndex].name }</h3>
        <div className="text">position:</div>
          <form className="form-inline">  
             <div className="form-group col-xs-2">
                <span className="text">&nbsp; x &nbsp;</span>
                <Input className="form-control" type="number" value={valx} onChange={(e) => this.onChangeX(e) }/> 
                <span className="text">&nbsp; y &nbsp;</span>
                <Input className="form-control" type="number" value={valy} onChange={(e) => this.onChangeY(e) }/>
            </div>
          </form>

        <div className="text">size:</div>
      <form className="form-inline">  
           <div className="form-group col-xs-2">
                 <span className="text"> &nbsp; h &nbsp; </span>
                 <Input className="form-control" type="number" value={valh}  onChange={(e) => this.onChangeH(e) }/>
                 <span className="text"> &nbsp; w &nbsp; </span>
                 <Input className="form-control" type="number" value={valw} onChange={(e) => this.onChangeW(e) }/>
             </div>
       </form>

        <div className="text">color:</div>
             <div className="colorContainer" >
             <Input type="color" onChange={(e) => this.ChangeColor(e.target.value)  } name="color" id="exampleColor" placeholder="color placeholder" />
              <div className="red commonColorStyle" onClick={(e) => this.ChangeColor("red")  }></div>&nbsp;
              <div className="blue commonColorStyle" onClick={(e) => this.ChangeColor("blue") }></div>&nbsp;
              <div className="green commonColorStyle" onClick={(e) => this.ChangeColor("green") }></div>&nbsp;
             </div>

    </div>
    );
  }
}

export default ObjectParams