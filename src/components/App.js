import React, { Component } from 'react';
import '.././App.css';
import ObjectList from './ObjectList'
import NewCanvas from './NewCanvas'
import ObjectParams from './ObjectParams'

class App extends Component {
   constructor(props) {
    super(props);
    this.state = {
     objects: [{},{
 				x:0,
 				y:0,
 				h:140,
 				w:140,
 				color:'blue',
 				name:'blue rect',
 				id:0
               },
               {
 				x:100,
 				y:100,
 				h:140,
 				w:140,
 				color:'red',
 				name:'red rect',
 				id:1
               }],
      currentActiveIndex: 0
    };
  }
 updateData = (value) => {
  this.setState( { objects: value });
 
}
updateIndex = (index) => {
	this.setState({currentActiveIndex: index});
	 console.log("pick index : " + index);
}
RemoveObj = (id) => {
  {/*
   if(this.state.currentActiveIndex == id){
      console.log("Can't remove current picked object");
       return;
   }
   */}
   var copy = this.state.objects.filter(obj => obj.id !== id);
   copy.map((obj, index) => obj.id = index);
   var lastIndex = copy[copy.length-1].id;
   this.setState({
     currentActiveIndex: lastIndex
   })
  this.setState({
        objects: copy
    })
}

  render() {
    return (
	<div className="container-fluid">
		<div className="row">
            <ObjectList
             objects={this.state.objects}
             RemoveObj={this.RemoveObj.bind(this)}
              updateData={this.updateData.bind(this)}
               updateIndex={this.updateIndex.bind(this)}/>
            <NewCanvas
             objects={this.state.objects}
              updateData={this.updateData.bind(this)} 
              updateIndex={this.updateIndex.bind(this)} />
            <ObjectParams
             objects={this.state.objects}
              currentActiveIndex={this.state.currentActiveIndex}
               updateData={this.updateData.bind(this)}/>
		</div>
	</div>
    );
  }
}

export default App;
