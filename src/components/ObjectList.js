import React from 'react'
import Obj from './Obj'
import { InputGroup, InputGroupAddon, Button, Input, ListGroup, ListGroupItem } from 'reactstrap'

class ObjectList extends React.Component {
   constructor(props) {
    super(props);
    this.state = {
      Objname: ""
    }
  }
  handleAdd(e){
    var copy = this.props.objects;
    var newId;
    console.log(this.props.objects.length)
    if(this.props.objects.length){
     newId = copy.slice(-1)[0].id;
     newId = newId + 1;
    } else {
     newId = 0;
    }
     var oName = this.state.Objname;
   if(oName){
     var newObj = {
       x:0,
       y:0,
       w:0,
       h:0,
       id:newId,
       color:"white",
       name: oName
     }
      this.props.updateIndex(newObj.id);
      copy.push(newObj);
      this.props.updateData(copy);
     this.setState({Objname: ""});
    }
  }

   handleChange(e){
      e.preventDefault();
    if (!e.target.value.length) {
      return;
    } else {
       this.setState({Objname: e.target.value});
    }
   }

  render() {
    return (
       <div className="col-lg-2">
         <form>
          <div className="text">List of objects:</div><br/>
              <ListGroup>
                   {this.props.objects.slice(1).map( (obj, index) => 
                     <Obj 
                       key={obj.id}
                       {...obj}
                       RemoveObj={() => this.props.RemoveObj(obj.id) }
                        onClick={() => this.props.updateIndex(obj.id)}/>
                   )}
                   <ListGroupItem>
                             <InputGroup>
                                    <InputGroupAddon addonType="append" className="">
                                      <Input className="form-control"  onChange={(e) => this.handleChange(e)}  type="text" placeholder="name" />
                                      <Button color="primary"  type="reset" onClick={(e) => this.handleAdd(e) }>Add</Button>
                                    </InputGroupAddon>
                             </InputGroup>
                   </ListGroupItem>
              </ListGroup>
          
           
        </form>
      </div>
    );
  }
}

export default ObjectList