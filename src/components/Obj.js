import React from 'react'
import { ListGroupItem, Button, ButtonGroup} from 'reactstrap'

class Obj extends React.Component {
	componentDidMount() {
        console.log(this.props.name);
    }
	render(){
	    return(
                  <ListGroupItem>
                  <ButtonGroup size="">
                   <Button   onClick={this.props.onClick} style={{ maxWidth: '70%', minWidth: '60%' }} >{this.props.name}</Button>
                   <Button color="info" className=""  onClick={this.props.RemoveObj} style={{ maxWidth: '30%', minWidth: '20%' }} >X</Button>
                  </ButtonGroup>
                 </ListGroupItem>
			  )
	}
}

export default Obj