import React from 'react'

class Canvas extends React.Component {
    componentDidMount() {
        this.ClearCanvas();
        this.DrawCanvas();
        console.log("componentDidMount Canvas component");
    }
    componentDidUpdate() {
        this.ClearCanvas();
        this.DrawCanvas();
    }

    DrawCanvasParams() {
        const ctx = this.refs.canvas.getContext('2d');
        ctx.fillStyle = this.props.objects.color;
        ctx.fillRect(this.props.objects.x, this.props.objects.y, this.props.objects.w, this.props.objects.h);
      }
       DrawCanvas() {
        const ctx = this.refs.canvas.getContext('2d');
        this.props.objects.map(obj =>{
        ctx.fillStyle = obj.color;
        ctx.fillRect(obj.x, obj.y, obj.w, obj.h);
        })
      }
    ClearCanvas(){
       const ctx = this.refs.canvas.getContext('2d');
       ctx.clearRect(0, 0, 700, 850);
    }

  render() {
    return (
       <div className="col-lg-8">
              <form>
                 <canvas ref="canvas" id="mainCanvas" onClick={(e) => console.log(e.pageX + '  pageX  ' + e.clientX + '  clientX  ' + e.screenX + '  screenX  ' + e.movementX + '  movementX  ')}>
                </canvas>
              </form>
    </div>
    );
  }
}

export default Canvas