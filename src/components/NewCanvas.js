import React, { Component } from "react";
import Konva from "konva";
import { render } from "react-dom";
import { Stage, Layer, Rect } from "react-konva";

class NewCanvas extends Component {
  constructor(props) {
    super(props);
    this.state = {
       currentActiveIndex: 0
    }
  }
  handleDragStart = e => {
       var obj = [...this.props.objects];
       obj.map( (o, i) => {
            if(o.x === e.target.attrs.x && o.y === e.target.attrs.y){
              this.setState({currentActiveIndex: i })
            }
       })
       console.log(this.state.currentActiveIndex)
  };
  handleDragEnd = e => {
      var obj = [...this.props.objects];
      var i = this.state.currentActiveIndex;
      obj[i].x = e.target.attrs.x;
      obj[i].y = e.target.attrs.y;
      this.props.updateData(obj);
      this.props.updateIndex(i);
  };
 
  render() {
    return (
    <div className="canvas">
      <Stage width={750} height={700} container={'.canvas'}>
        <Layer>
          {[...this.props.objects].slice(1).map(obj => (
            <Rect
              key={obj.id}
              x={obj.x}
              y={obj.y}
              fill={obj.color}
              width={obj.w}
              height={obj.h}
              draggable
              onDragStart={this.handleDragStart}
              onDragEnd={this.handleDragEnd}
            />
          ))}
        </Layer>
      </Stage>
    </div>
    );
  }
}

export default NewCanvas